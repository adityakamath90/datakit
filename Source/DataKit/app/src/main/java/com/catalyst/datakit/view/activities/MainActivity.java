package com.catalyst.datakit.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.catalyst.datakit.R;
import com.catalyst.datakit.models.Image;
import com.catalyst.datakit.models.TopNav;
import com.catalyst.datakitlib.core.DataKitHelper;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private TextView queryResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        queryResult = (TextView) findViewById(R.id.query_result);

        new Thread() {
            @Override
            public void run() {
                InputStream is = getResources().openRawResource(R.raw.sample_response_1);
                Writer writer = new StringWriter();
                char[] buffer = new char[1024];
                try {
                    try {
                        Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                        int n;
                        while ((n = reader.read(buffer)) != -1) {
                            writer.write(buffer, 0, n);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        // Do nothing
                    }
                }

                String jsonString = writer.toString();
                Gson gson = new Gson();
                TopNav topNav = gson.fromJson(jsonString, TopNav.class);
                Log.v(TAG, topNav.toString());
            }
        }.start();
    }

    public void insert(View view) {


        Image image = new Image();
        image.setAlt("xyz");
        image.setHeight("12");
        image.setWidth("12");
        image.setSrc("saasdad");
        image.setId(1);

        Image image1 = new Image();
        image1.setAlt("abc");
        image1.setHeight("122");
        image1.setWidth("435");
        image1.setSrc("/faf/afaf");
        image1.setId(2);

        try {
            DataKitHelper.insert(image);
            boolean isInserted = DataKitHelper.insert(image1);

            if (isInserted) {
                Toast.makeText(this, "Insert successfull", Toast.LENGTH_SHORT).show();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void update(View view) {

        Image image = new Image();
        image.setAlt("xyz");
        image.setHeight("12");
        image.setWidth("12");
        image.setSrc("alternative");
        image.setId(1);
        boolean status = DataKitHelper.update(image, null, null);
        if (status) {
            Toast.makeText(this, "Updated successfull", Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(View view) {

        boolean status = DataKitHelper.delete(Image.class, "id =?", "1");
        if (status) {
            Toast.makeText(this, "Deleted successfull", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteAll(View view) {
        boolean status = DataKitHelper.deleteAll(Image.class);
        if (status) {
            Toast.makeText(this, "Deleted successfull", Toast.LENGTH_SHORT).show();
        }
    }

    public void select(View view) {
        try {
            Image image = DataKitHelper.select(Image.class, null, null);
            if (image != null) {
                Toast.makeText(this, image.toString(), Toast.LENGTH_SHORT).show();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
            Toast.makeText(this, "Select unsuccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    public void insertNew(View view) {
        Image image = new Image();
        image.setAlt("abc");
        image.setHeight("122");
        image.setWidth("435");
        image.setSrc("/faf/afaf");
        image.setId(90);

        try {
            boolean isInserted = DataKitHelper.insert(image);
            if (isInserted) {
                Toast.makeText(this, "Insert successfull", Toast.LENGTH_SHORT).show();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
