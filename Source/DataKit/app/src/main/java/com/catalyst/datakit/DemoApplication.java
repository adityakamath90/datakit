package com.catalyst.datakit;

import android.app.Application;
import android.util.Log;

import com.catalyst.datakitlib.core.DataKitConfiguration;

/**
 * Created by mukund.d on 16-07-2016.
 */
public class DemoApplication extends Application {

    private static final String TAG = "DemoApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        Log.v(TAG, "in onCreate");

        new Thread() {
            @Override
            public void run() {
                DataKitConfiguration.builder(DemoApplication.this).with(1).with(getString(R
                        .string.models_package_name)).build().init();
            }
        }.start();
    }
}
