package com.catalyst.datakit.models;

/**
 * Created by mukund.d on 16-07-2016.
 */
public class TopNav {
    private Data[] data;

    private String type;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ClassPojo [data = " + data + ", type = " + type + "]";
    }
}
