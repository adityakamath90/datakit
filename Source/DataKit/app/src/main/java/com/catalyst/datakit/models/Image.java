package com.catalyst.datakit.models;

import com.catalyst.datakitlib.DataModel;
import com.catalyst.datakitlib.annotation.DbTable;

/**
 * Created by mukund.d on 16-07-2016.
 */
@DbTable(name = "image")
public class Image extends DataModel {
    private String height;

    private String alt;

    private String width;

    private String src;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String toString() {
        return "ClassPojo [height = " + height + ", alt = " + alt + ", width = " + width + ", src = " + src + "]";
    }
}
