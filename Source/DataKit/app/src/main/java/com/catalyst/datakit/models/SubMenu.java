package com.catalyst.datakit.models;

import com.catalyst.datakitlib.DataModel;
import com.catalyst.datakitlib.annotation.DbTable;

/**
 * Created by mukund.d on 16-07-2016.
 */
@DbTable(name = "sub_menu")
public class SubMenu extends DataModel {

    private String fallback;

    private String text;

    private String sectionBreak;

    private String sequence;

    private String link;

    private String path;

    private Image image;

    private String moduleAssoc;

    private String type;

    private long dataId;

    public String getFallback() {
        return fallback;
    }

    public void setFallback(String fallback) {
        this.fallback = fallback;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSectionBreak() {
        return sectionBreak;
    }

    public void setSectionBreak(String sectionBreak) {
        this.sectionBreak = sectionBreak;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getModuleAssoc() {
        return moduleAssoc;
    }

    public void setModuleAssoc(String moduleAssoc) {
        this.moduleAssoc = moduleAssoc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getDataId() {
        return dataId;
    }

    public void setDataId(long dataId) {
        this.dataId = dataId;
    }

    @Override
    public String toString() {
        return "ClassPojo [fallback = " + fallback + ", text = " + text + ", sectionBreak = " + sectionBreak + ", sequence = " + sequence + ", link = " + link + ", path = " + path + ", image = " + image + ", moduleAssoc = " + moduleAssoc + ", type = " + type + "]";
    }
}
