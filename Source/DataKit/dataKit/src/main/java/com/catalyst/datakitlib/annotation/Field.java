package com.catalyst.datakitlib.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for formatting field name
 * 
 * Created by mukund.d on 16-07-2016.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Field {
    String name() default "";

    boolean isNull() default true;

    String defaultValue() default "";
}
