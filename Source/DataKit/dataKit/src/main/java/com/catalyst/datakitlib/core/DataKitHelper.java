package com.catalyst.datakitlib.core;

import com.catalyst.datakitlib.DataModel;

import java.util.List;

/**
 * Created by mukund.d on 16-07-2016.
 *
 * DataKitHelper contains method for performing CRUD operations
 */
public class DataKitHelper {

    DataKitHelper() {
    }

    /**
     * Method to save object to table
     *
     * @param model Model to save
     * @param <T>   Object of type @DataModel
     * @return true for successful save
     */
    public static <T extends DataModel> boolean insert(T model) throws IllegalAccessException {
        return DataKitManager.getInstance().insert(model);
    }

    /**
     * Method to do batch operation on multiple insertions. Note: Model should be same for all the values
     *
     * @param models : List of values for the same table
     * @param <T>    : Object of type @DataModel
     * @return true for successful save
     */
    public static <T extends DataModel> boolean insert(List<T> models) throws IllegalAccessException {
        return DataKitManager.getInstance().insert(models);
    }

    /**
     * Method to find record in table
     *
     * @param t     Model to find
     * @param where where clause
     * @param args  column names
     * @param <T>   Object of type @DataModel
     * @return true if found
     */
    public static <T extends DataModel> T select(Class<T> t, String where, String... args) throws InstantiationException {
        return DataKitManager.getInstance().select(t, where, args);
    }

    /**
     * Method to update record in table
     *
     * @param model Model to update
     * @param where where clause
     * @param args  column names
     * @param <T>   Object of type @DataModel
     * @return true for successful updation
     */
    public static <T extends DataModel> boolean update(T model, String where, String... args) {
        return DataKitManager.getInstance().update(model);
    }

    /**
     * Method to delete record from table
     *
     * @param model Model to delete
     * @param where where clause
     * @param args  column names
     * @param <T>   Object of type @DataModel
     * @return true for successful deletion
     */
    public static <T extends DataModel> boolean delete(Class<T> model, String where, String...
            args) {
        return DataKitManager.getInstance().delete(model, where, args);
    }

    /**
     * Method to delete all record from table
     *
     * @param model Model to delete
     * @param <T>   Object of type @DataModel
     * @return true for successful deletion
     */
    public static <T extends DataModel> boolean deleteAll(Class<T> model) {
        return DataKitManager.getInstance().deleteAll(model);
    }

}
