package com.catalyst.datakitlib.core.data;

/**
 * Created by rankushkumar.a on 16-07-2016.
 */

/**
 * It defines a column of the table.
 */
public class ColumnProp {

    public final String columnName;
    public final boolean isPrimaryKey;
    public final boolean isNotNull;
    public final String columnType;
    public final boolean isAutoIncrement;
    public final String defaultValue;

    public ColumnProp(String columnName, boolean isPrimaryKey, boolean isNotNull, String defaultValue, String columnType, boolean isAutoIncrement) {
        this.columnName = columnName;
        this.isPrimaryKey = isPrimaryKey;
        this.isNotNull = isNotNull;
        this.defaultValue = defaultValue;
        this.columnType = columnType;
        this.isAutoIncrement = isAutoIncrement;
    }
}
