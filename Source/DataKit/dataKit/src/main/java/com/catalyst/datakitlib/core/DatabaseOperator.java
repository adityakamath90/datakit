package com.catalyst.datakitlib.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.catalyst.datakitlib.DataModel;
import com.catalyst.datakitlib.core.data.ColumnProp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by rankushkumar.a on 16-07-2016.
 */

public class DatabaseOperator extends SQLiteOpenHelper {

    private static final String TAG = DatabaseOperator.class.getSimpleName();
    public static final String DATABASE_NAME = "DataKit.db";
    public static final String NAME = "name";


    private IDatabaseListener mDatabaseListener;
    private SQLiteDatabase mDatabaseHandle;

    private static final String mStringAutoInc = " AUTOINCREMENT ";
    private static final String mStringPrimaryKey = " PRIMARY KEY ";
    private String mStringNotNull = " NOT NULL ";
    private String mStringCreateTable = " CREATE TABLE ";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS ";
    private static final String SELECT_TABLE_NAMES = "SELECT name FROM sqlite_master WHERE "
            +"type='table' and name NOT IN('android_metadata','sqlite_sequence')";

    private static final String whiteSpace = " ";
    public DatabaseOperator(Context context, int version) {
        super(context, DATABASE_NAME, null, version);
        mDatabaseHandle = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mDatabaseHandle = db;
        if (mDatabaseListener != null) {
            mDatabaseListener.onDBCreate(this);
            return;
        }
       // DataKitManager.getInstance().onDBCreate(this);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mDatabaseHandle = db;
        if (newVersion > oldVersion) {
            if (mDatabaseListener != null) {
                mDatabaseListener.onDBUpgrade(this);
                return;
            }
         //   DataKitManager.getInstance().onDBUpgrade(this);
        }
    }


    /**
     * Create the required tables in database.
     * Hashmap : Key represents the table name and Value represents the list of columns
     *
     * @param tables: Tables to be created
     */
    protected void createDatabase(HashMap<String, List<ColumnProp>> tables) {
        boolean isPrimaryKeyAvailable = false;

        for (Map.Entry<String, List<ColumnProp>> pair : tables.entrySet()) {
            String tableName = pair.getKey();
            List<ColumnProp> columns = pair.getValue();

            StringBuilder columnBuilder = new StringBuilder();

            for (int i = 0; i < columns.size(); i++) {
                ColumnProp col = columns.get(i);
                columnBuilder.append(whiteSpace+col.columnName+whiteSpace).append(whiteSpace+col.columnType+whiteSpace+whiteSpace);
                if (col.isPrimaryKey) {
                    isPrimaryKeyAvailable = true;
                    columnBuilder.append(whiteSpace+mStringPrimaryKey+whiteSpace);
                    if (col.isAutoIncrement) {
                        columnBuilder.append(whiteSpace+mStringAutoInc+whiteSpace);
                    }
                }
                if (col.isNotNull) {
                    columnBuilder.append(mStringNotNull);
                    if (col.defaultValue != null) {
                        columnBuilder.append(whiteSpace+col.defaultValue+whiteSpace).append(' ');
                    }
                }
                if (i < columns.size()-1) {
                    columnBuilder.append(" ,");
                }

            }

            getWritableDatabase().execSQL(mStringCreateTable + tableName + "(" + columnBuilder + " )");
        }

    }



    /**
     * Upgrade the database. Firstly it will check for new and deleted tables from database.
     * For new tables: Creates new tables. for deleted tables: delete the corresponding tables from database.
     * Then for each table it will check for new and deleted columns. And perform alter operation.
     *
     * @param tables : new Database tables
     */
    protected void upgradeDatabase(HashMap<String, List<ColumnProp>> tables) {

        ArrayList<String> oldTableNames = getTableListAndModifyTable(tables);
        modifyColumnFields(tables, oldTableNames);

    }

    private void modifyColumnFields(HashMap<String, List<ColumnProp>> tables, ArrayList<String> oldTableNames) {
        for (String tableName : oldTableNames) {
            Cursor dbCursor = mDatabaseHandle.query(tableName, null, null, null, null, null, null);
            ArrayList<String> oldColumnList = new ArrayList<>();
            oldColumnList.addAll(Arrays.asList(dbCursor
                    .getColumnNames()));

            if (dbCursor != null) {
                dbCursor.close();
            }

            List<ColumnProp> newColumns = tables.get(tableName);

            ArrayList<String> newColumnList = new ArrayList<>();
            for (ColumnProp columns : newColumns) {
                newColumnList.add(columns.columnName);
            }

            if (changeInFields(tableName, oldColumnList, newColumnList)) {
                if (isTableExists(mDatabaseHandle, tableName)) {

                    //Delete columns
                    if (!oldColumnList.isEmpty()) {
                        String[] columnList = new String[newColumnList.size()];
                        Cursor cursor = mDatabaseHandle.query(tableName, columnList, null, null, null,
                                null, null);
                        dropTable(tableName);
                        HashMap<String, List<ColumnProp>> newTable = new HashMap<>();
                        newTable.put(tableName, tables.get(tableName));
                        createDatabase(newTable);

                        ArrayList<ContentValues> contentValues = new ArrayList<ContentValues>();
                        ContentValues map;
                        if (cursor.moveToFirst()) {
                            do {
                                map = new ContentValues();
                                DatabaseUtils.cursorRowToContentValues(cursor, map);
                                contentValues.add(map);
                            } while (cursor.moveToNext());
                        }

                        cursor.close();
                        insert(tableName, contentValues);
                    }

                    //ADD columns
                    if (!newColumnList.isEmpty()) {
                        for (ColumnProp columnProperty : newColumns)
                            if (!existsColumnInTable(mDatabaseHandle, tableName, columnProperty.columnName))
                                alterTableAddColumns(mDatabaseHandle, tableName, columnProperty);
                    }
                }
            }
        }
    }

    private ArrayList<String> getTableListAndModifyTable(HashMap<String, List<ColumnProp>> tables) {
        ArrayList<String> oldTableNames = new ArrayList<>();
        Cursor c = mDatabaseHandle.rawQuery(SELECT_TABLE_NAMES, null);

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                oldTableNames.add(c.getString(c.getColumnIndex(NAME)));
                c.moveToNext();
            }
        }

        if (c != null)
            c.close();

        Set<String> tableKeys = tables.keySet();
        List<String> newTableKeys = new ArrayList<>();
        newTableKeys.addAll(tableKeys);


        if (changeInTables(oldTableNames, newTableKeys)) {

            if (!oldTableNames.isEmpty()) {
                for (String oldTable : oldTableNames) {
                    dropTable(oldTable);
                }
            }

            if (!newTableKeys.isEmpty()) {
                for (String tableKey : newTableKeys) {
                    List<ColumnProp> columnProps = tables.get(tableKey);
                    HashMap<String, List<ColumnProp>> hashMap = new HashMap<>();
                    hashMap.put(tableKey, columnProps);
                    createDatabase(hashMap);
                }
            }

        }
        return oldTableNames;
    }

    private boolean changeInTables(List<String> oldTables, List<String> newTables) {
        return DataKitComparator.compareFieldDiff(oldTables, newTables);
    }

    private boolean changeInFields(String tableName, List<String> oldFields, List<String>
            newFields) {
        return DataKitComparator.compareFieldDiff(oldFields, newFields);
    }

    /**
     * Method to insert record to table
     *
     * @param @param tableName Table name to which record has to be inserted
     * @param values Values to be saved
     * @param <T>    Object of type @DataModel
     * @return true for successful save
     */
    <T extends DataModel> boolean insert(String tableName, ContentValues values) {
        long rowsEffected = mDatabaseHandle.insert(tableName, null, values);
        return rowsEffected > 0;
    }

    /**
     * Method to insert multiple record to table
     *
     * @param tableName Table name to which record has to be inserted
     * @param valueList List of content values
     * @param <T>       Object of type @DataModel
     * @return true for successful insertion
     */
    <T extends DataModel> boolean insert(String tableName, List<ContentValues> valueList) {
        for (ContentValues value : valueList) {
            long rowsAffected = mDatabaseHandle.insert(tableName, null, value);
            if (rowsAffected < 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to find record in table
     *
     * @param tableName Table name from which record has to be retrieved
     * @param where     where clause
     * @param args      column names
     * @param <T>       Object of type @DataModel
     * @return resultant cursor
     */
    <T extends DataModel> Cursor select(String tableName, String where, String... args) {
        Cursor cursor = mDatabaseHandle.query(tableName, null, where, args, null, null, null);
        return cursor;
    }

    /**
     * Method to update record in table
     *
     * @param tableName     Table name from where update has to happen
     * @param contentValues Content value to update
     * @param where         where clause
     * @param args          column names
     * @param <T>           Object of type @DataModel
     * @return true for successful updation
     */
    <T extends DataModel> boolean update(String tableName, ContentValues contentValues, String where, String... args) {
        int rowsEffected = mDatabaseHandle.update(tableName, contentValues, where, args);
        return rowsEffected > 0;
    }

    /**
     * Method to delete record from table
     *
     * @param tableName Table name from where deletion has to happen
     * @param where     where clause
     * @param args      column names
     * @param <T>       Object of type @DataModel
     * @return true for successful deletion
     */
    <T extends DataModel> boolean delete(String tableName, String where, String... args) {
        int rowsEffected = mDatabaseHandle.delete(tableName, where, args);
        return rowsEffected > 0;
    }

    /**
     * Method to delete all record from table
     *
     * @param tableName Table name from where deletion has to happen
     * @param <T>       Object of type @DataModel
     * @return true for successful deletion
     */
    <T extends DataModel> boolean deleteAll(String tableName) {
        int rowsEffected = mDatabaseHandle.delete(tableName, null, null);
        return rowsEffected > 0;
    }

    /**
     * Method to drop table
     *
     * @param tablename Name of table
     * @return
     */
    void dropTable(String tablename) {
        mDatabaseHandle.execSQL(DROP_TABLE + tablename);
    }

    /**
     * Register the listener for getting the callbacks of database creation and upgrade
     *
     * @param listener : IDatabaseListener instance
     */

    protected void registerDatabaseListener(IDatabaseListener listener) {
        mDatabaseListener = listener;
    }

    /**
     * Unregister the listener for database creation and upgrade callbacks
     */
    protected void unRegisterDatabaseListener() {
        mDatabaseListener = null;
    }

    /**
     * An Interface , which will provides the callbacks for creation and upgradation of the database
     */
    interface IDatabaseListener {
        void onDBCreate(DatabaseOperator operator);

        void onDBUpgrade(DatabaseOperator operator);

    }

    boolean isTableExists(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }

    void alterTableAddColumns(SQLiteDatabase db, String table, ColumnProp column) {
        db.execSQL("ALTER TABLE " + table + " ADD COLUMN " + column.columnName + " " + column
                .columnType);
    }

    private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        Cursor mCursor = null;
        try {
            // Query 1 row
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

            // getColumnIndex() gives us the index (0 to ...) of the column - otherwise we get a -1
            if (mCursor.getColumnIndex(columnToCheck) != -1)
                return true;
            else
                return false;

        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }
}