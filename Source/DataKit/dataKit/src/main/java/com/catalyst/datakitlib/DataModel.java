package com.catalyst.datakitlib;

import com.catalyst.datakitlib.annotation.Field;
import com.catalyst.datakitlib.annotation.Primary;

/**
 * This is the abstract model class. TO be extended by all those POJOs to be qualified for DataKit ORM.
 * This class declares a private field 'id', which by default will be considered for primary key column.
 * <p/>
 * Created by mukund.d on 16-07-2016.
 */
public abstract class DataModel {

    @Primary
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
