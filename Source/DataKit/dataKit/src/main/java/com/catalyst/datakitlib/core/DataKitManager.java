package com.catalyst.datakitlib.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.catalyst.datakitlib.DataModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mukund.d on 16-07-2016.
 */
class DataKitManager implements DatabaseOperator.IDatabaseListener {

    private static DataKitManager INSTANCE = new DataKitManager();
    private DataKitConfiguration mConfiguration;
    private DatabaseOperator mOperator;
    private Context mContext;

    private DataKitManager() {
    }

    static DataKitManager getInstance() {
        return INSTANCE;
    }

    void init(DataKitConfiguration configuration, int version) {
        mConfiguration = configuration;
        mContext = configuration.mContext;
        mOperator = new DatabaseOperator(mContext, version);
        mOperator.registerDatabaseListener(this);
    }

    /**
     * Method to insert object to table
     *
     * @param model Model to insert
     * @param <T>   Object of type @DataModel
     * @return true for successful save
     */
    <T extends DataModel> boolean insert(T model) throws IllegalAccessException {
        String tableName = TableMapper.getTableName(model.getClass());
        ContentValues values = TableMapper.getContentValue(model);
        return mOperator.insert(tableName, values);
    }


    /**
     * Method to do batch operation on multiple insertions. Note: Model should be same for all the values
     *
     * @param models : List of values for the same table
     * @param <T>    : Object of type @DataModel
     * @return true for successful save
     */
    <T extends DataModel> boolean insert(List<T> models) throws IllegalAccessException {
        onDBCreate(null);
        String tableName = TableMapper.getTableName(models.get(0).getClass());
        List<ContentValues> values = new ArrayList<>(models.size());
        for (T model : models) {
            values.add(TableMapper.getContentValue(model));
        }
        return mOperator.insert(tableName, values);
    }


    /**
     * Method to find record in table
     *
     * @param clazz Model to find
     * @param where where clause
     * @param args  column names
     * @param <T>   Object of type @DataModel
     * @return true if found
     */
    <T extends DataModel> T select(Class<T> clazz, String where, String... args) throws InstantiationException {
        String tableName = TableMapper.getTableName(clazz);
        Cursor resultSet = mOperator.select(tableName, where, args);
        T mode = TableMapper.getModel(clazz, resultSet);
        resultSet.close();
        return mode;
    }

    /**
     * Method to update record in table
     *
     * @param model Model to update
     * @param <T>   Object of type @DataModel
     * @return true for successful updation
     */
    <T extends DataModel> boolean update(T model) {
        String tableName = TableMapper.getTableName(model.getClass());
        ContentValues values = TableMapper.getContentValue(model);
        Map.Entry<String, String> primaryKey = TableMapper.getPrimaryKey(model);
        return mOperator.update(tableName,values, primaryKey.getKey()+" =? ", primaryKey.getValue() );
    }

    /**
     * Method to delete record from table
     *
     * @param model Model to delete
     * @param where where clause
     * @param args  column names
     * @param <T>   Object of type @DataModel
     * @return true for successful deletion
     */
    <T extends DataModel> boolean delete(Class<T> model, String where, String... args) {
        String tableName = TableMapper.getTableName(model);
        return mOperator.delete(tableName, where, args);
    }

    /**
     * Method to delete all record from table
     *
     * @param model Model to delete
     * @param <T>   Object of type @DataModel
     * @return true for successful deletion
     */
    <T extends DataModel> boolean deleteAll(Class<T> model) {
        String tableName = TableMapper.getTableName(model);
        return mOperator.deleteAll(tableName);
    }

    @Override
    public void onDBCreate(DatabaseOperator operator) {
        if (mOperator == null){
            mOperator =  operator;
        }
        mOperator.createDatabase(TableMapper.getAllTables(mConfiguration.mModelPackage, mContext));
    }

    @Override
    public void onDBUpgrade(DatabaseOperator operator) {
        if (mOperator == null){
            mOperator =  operator;
        }
        mOperator.upgradeDatabase(TableMapper.getAllTables(mConfiguration.mModelPackage, mContext));
    }
}
