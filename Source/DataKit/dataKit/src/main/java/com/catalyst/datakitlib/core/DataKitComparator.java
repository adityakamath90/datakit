package com.catalyst.datakitlib.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aditya.k on 16-07-2016.
 */

/**
 * DataKitComparator compares Table names of table fields
 * for changes
 */
class DataKitComparator {

    private DataKitComparator() {

    }

    /**
     * Removes all common table
     *
     * @param oldList Old table list
     * @param newList New table list
     */
    static void computeTableDiff(List<String> oldList, List<String> newList) {
        List<String> common = new ArrayList<>(oldList);
        common.retainAll(newList);

        oldList.removeAll(common);
        newList.removeAll(common);
    }

    /**
     * Removes all common fields
     *
     * @param oldList Old table list
     * @param newList New table list
     */
    static boolean  compareFieldDiff(List<String> oldList, List<String> newList) {
        List<String> common = new ArrayList<>(oldList);
        common.retainAll(newList);

        oldList.removeAll(common);
        newList.removeAll(common);

        return (oldList.size()>0)||(newList.size()>0);
    }

}
