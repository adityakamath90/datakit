package com.catalyst.datakitlib.core;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.catalyst.datakitlib.DataModel;
import com.catalyst.datakitlib.annotation.DbTable;
import com.catalyst.datakitlib.annotation.Exclude;
import com.catalyst.datakitlib.annotation.Primary;
import com.catalyst.datakitlib.core.data.ColumnProp;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

/**
 * Created by mukund.d on 16-07-2016.
 */
public class TableMapper {

    private static final char PKG_SEPARATOR = '.';

    private static final char DIR_SEPARATOR = '/';

    private static final String CLASS_FILE_SUFFIX = ".class";

    private static final String INSTANT_RUN_DEX_DIR_PATH = "files/instant-run/dex/";

    private static final String SECONDARY_FOLDER_NAME = "code_cache" + File.separator + "secondary-dexes";

    private static final String BAD_PACKAGE_ERROR = "Unable to get resources from path '%s'. Are you sure the package '%s' exists?";

    private static final String ILLEGAL_STATE = "Unable to access properties of %s at this time";

    private static final String TAG = "TableMapper";

    public static <T extends DataModel> HashMap<String, List<ColumnProp>> getAllTables(String scannedPackage, Context context) {
        HashMap<String, List<ColumnProp>> tablesMap = null;
        List<Class<DataModel>> tables = getTables(scannedPackage, context);
        if (tables != null && !tables.isEmpty()) {
            Class<DataModel>[] array = new Class[tables.size()];
            tablesMap = TableMapper.getTablesMap(tables.toArray(array));
        }
        Log.v(TAG, "Tables : " + tablesMap);
        return tablesMap;
    }

    private static <T extends DataModel> List<Class<T>> getTables(String scannedPackage, Context context) {

        List<Class<T>> classes = new ArrayList<>();
        try {
            List<String> paths = getDexes(context);
            for (String path : paths) {
                DexFile df = new DexFile(path);
                for (Enumeration<String> iter = df.entries(); iter.hasMoreElements(); ) {
                    String className = iter.nextElement();
                    Log.v(TAG, "Class Name : " + className);
                    if (className.contains(scannedPackage)) {
                        Class<T> modelClass = (Class<T>) Class.forName(className);
                        DbTable annotations = modelClass.getAnnotation(DbTable.class);
                        if (annotations == null) {
                            continue;
                        }
                        classes.add((Class<T>) (Class.forName(className)));
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }

    private static List<String> getDexes(Context context) throws PackageManager.NameNotFoundException {
        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
        File sourceApk = new File(applicationInfo.sourceDir);
        File dexDir = new File(applicationInfo.dataDir, SECONDARY_FOLDER_NAME);
        File instantRunDir = new File(applicationInfo.dataDir, INSTANT_RUN_DEX_DIR_PATH); //default instant-run dir

        List<String> sourcePaths = new ArrayList<>();
        sourcePaths.add(applicationInfo.sourceDir); //add the default apk path

        if (instantRunDir.exists()) { //check if app using instant run
            for (final File dexFile : instantRunDir.listFiles()) { //add all sources from instan-run
                sourcePaths.add(dexFile.getAbsolutePath());
            }
        }

        /*
        //the prefix of extracted file, ie: test.classes
        String extractedFilePrefix = sourceApk.getName() + EXTRACTED_NAME_EXT;
        //the total dex numbers
        int totalDexNumber = getMultiDexPreferences().getInt(KEY_DEX_NUMBER, 1);

        for (int secondaryNumber = 2; secondaryNumber <= totalDexNumber; secondaryNumber++) {
            //for each dex file, ie: test.classes2.zip, test.classes3.zip...
            String fileName = extractedFilePrefix + secondaryNumber + EXTRACTED_SUFFIX;
            File extractedFile = new File(dexDir, fileName);
            if (extractedFile.isFile()) {
                sourcePaths.add(extractedFile.getAbsolutePath());
                //we ignore the verify zip part
            }
        }
        */

        return sourcePaths;
    }

    private static <T extends DataModel> HashMap<String, List<ColumnProp>> getTablesMap(Class<T>... dataModels) {
        HashMap<String, List<ColumnProp>> map = new HashMap<>();

        for (Class<?> clazz : dataModels) {
            boolean notAdded = true;
            boolean defaultPk = true;
            List<ColumnProp> columns = new ArrayList<>();
            while (DataModel.class.isAssignableFrom(clazz)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.getAnnotation(Exclude.class) != null) {
                        continue;
                    }
                    String name = getColumnName(field);
                    boolean isPrimary = (field.getAnnotation(Primary.class) != null);
                    if (isPrimary) {
                        if (DataModel.class != clazz) {
                            defaultPk = false;
                        } else {
                            if (!defaultPk) {
                                continue;
                            }
                        }
                    }
                    boolean isNotNull = false;
                    String type = getType(field.getType());
                    if (TextUtils.isEmpty(type)) {
                        continue;
                    }
                    String defaultValue = null;

                    com.catalyst.datakitlib.annotation.Field annotation = field.getAnnotation(com.catalyst.datakitlib.annotation.Field.class);
                    if (annotation != null) {
                        isNotNull = annotation.isNull();
                        if (isNotNull) {
                            defaultValue = annotation.defaultValue();
                        }
                        String annName = annotation.name().trim();
                        if (annName.length() > 0) {
                            name = annotation.name();
                        }
                    }

                    ColumnProp columnProp = new ColumnProp(name, isPrimary, isNotNull, defaultValue, type, isPrimary ? defaultPk : false);
                    columns.add(columnProp);
                }
                if (notAdded) {
                    DbTable dbTable = clazz.getAnnotation(DbTable.class);
                    map.put(dbTable.name(), columns);
                    notAdded = false;
                }
                clazz = clazz.getSuperclass();
            }
        }

        return map;
    }

    private static String getColumnName(Field field) {
        String name = field.getName();
        com.catalyst.datakitlib.annotation.Field annotation = field.getAnnotation(com.catalyst.datakitlib.annotation.Field.class);
        if (annotation != null) {
            String annName = annotation.name().trim();
            if (annName.length() > 0) {
                name = annotation.name();
            }
        }
        return name;
    }

    private static String getType(Class<?> type) {
        if (type == String.class) {
            return "TEXT";
        }
        if (type == Long.class || type == Integer.class || type == Byte.class || type == Boolean.class) {
            return "INTEGER";
        }
        if (type == Float.class || type == Double.class) {
            return "REAL";
        }
        if (type == byte[].class) {
            return "BLOB";
        }

        switch (type.getName()) {
            case "long":
            case "int":
            case "boolean":
                return "INTEGER";
            case "float":
            case "double":
                return "REAL";
        }
        return null;
    }

    public static <T extends DataModel> T getModel(Class<T> clazz, Cursor c) throws InstantiationException {
        T t = null;
        List<T> models = getModelsInternal(clazz, c, true);
        if (models != null && !models.isEmpty()) {
            t = models.get(0);
        }
        return t;
    }

    public static <T extends DataModel> List<T> getModels(Class<T> clazz, Cursor c) throws InstantiationException {
        return getModelsInternal(clazz, c, false);
    }

    private static <T extends DataModel> List<T> getModelsInternal(Class<T> clazz, Cursor c, boolean single) throws InstantiationException {
        List<T> list = null;
        if (c.moveToFirst()) {
            list = new ArrayList<>();
            do {
                T t = null;
                HashMap<String, List<ColumnProp>> tablesMap = getTablesMap(clazz);
                Collection<List<ColumnProp>> values = tablesMap.values();
                if (values.size() > 0) {
                    List<ColumnProp> columnProps = values.iterator().next();
                    try {
                        t = clazz.newInstance();
                        for (ColumnProp column : columnProps) {
                            String name = column.columnName;
                            Field field = findField(clazz, name);
                            if (field != null) {
                                fillType(field, t, c, name);
                            }
                        }
                        list.add(t);
                    } catch (InstantiationException e) {
                        throw new InstantiationException(String.format(ILLEGAL_STATE, clazz.getSimpleName()));
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(String.format(ILLEGAL_STATE, clazz.getSimpleName()));
                    }
                }
                if (single) {
                    break;
                }
            } while (c.moveToNext());
        }
        return list;
    }

    private static <T extends DataModel> Field findField(Class<?> clazz, String name) {
        while (DataModel.class.isAssignableFrom(clazz)) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                String type = getType(field.getType());
                String fieldName = field.getName();
                com.catalyst.datakitlib.annotation.Field annotation = field.getAnnotation(com.catalyst.datakitlib.annotation.Field.class);
                if (annotation != null) {
                    String annName = annotation.name().trim();
                    if (annName.length() > 0) {
                        name = annotation.name();
                    }
                }
                if (name.equalsIgnoreCase(fieldName)) {
                    return field;
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;
    }

    private static <T extends DataModel> void fillType(Field field, T t, Cursor c, String name) throws IllegalAccessException {
        boolean accessible = field.isAccessible();
        if (!accessible) {
            field.setAccessible(true);
        }

        Class<?> type = field.getType();

        int index = c.getColumnIndex(name);

        if (type == String.class) {
            field.set(t, c.getString(index));
        }
        if (type == Long.class) {
            field.set(t, c.getLong(index));
        }
        if (type == Integer.class || type == Byte.class) {
            field.set(t, c.getInt(index));
        }
        if (type == Byte.class) {
            field.set(t, c.getInt(index));
        }
        if (type == Boolean.class) {
            field.set(t, c.getInt(index) > 0);
        }
        if (type == Float.class) {
            field.set(t, c.getFloat(index));
        }
        if (type == Double.class) {
            field.set(t, c.getDouble(index));
        }
        if (type == byte[].class) {
            field.set(t, c.getBlob(index));
        }

        switch (type.getName()) {
            case "long":
                field.setLong(t, c.getLong(index));
                break;
            case "int":
                field.setInt(t, c.getInt(index));
                break;
            case "boolean":
                field.setBoolean(t, c.getInt(index) > 0);
                break;
            case "float":
                field.setFloat(t, c.getFloat(index));
                break;
            case "double":
                field.setDouble(t, c.getDouble(index));
                break;
        }

        field.setAccessible(accessible);
    }

    public static <T extends DataModel> ContentValues getContentValue(T t) {

        ContentValues contentValues = new ContentValues();
        Class<?> clazz = t.getClass();
        while (DataModel.class.isAssignableFrom(clazz)) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.getAnnotation(Exclude.class) != null) {
                    continue;
                }
                String name = getColumnName(field);
                com.catalyst.datakitlib.annotation.Field annotation = field.getAnnotation(com.catalyst.datakitlib.annotation.Field.class);
                if (annotation != null) {
                    String annName = annotation.name().trim();
                    if (annName.length() > 0) {
                        name = annotation.name();
                    }
                }
                boolean accessable = field.isAccessible();
                field.setAccessible(true);
                Object o;
                try {
                    o = field.get(t);
                } catch (IllegalAccessException e) {
                    throw new IllegalStateException(String.format(ILLEGAL_STATE, t.getClass().getSimpleName()));
                }
                String type = getType(field.getType());
                field.setAccessible(accessable);

                fillType(o, contentValues, name);
            }
            clazz = clazz.getSuperclass();
        }
        return contentValues;
    }

    private static <T extends DataModel> void fillType(Object o, ContentValues contentValues, String name) {

        Class<?> type = o.getClass();

        if (type == String.class) {
            contentValues.put(name, o.toString());
        }
        if (type == Long.class) {
            contentValues.put(name, Long.valueOf(o.toString()));
        }
        if (type == Integer.class || type == Byte.class) {
            contentValues.put(name, Integer.valueOf(o.toString()));
        }
        if (type == Byte.class) {
            contentValues.put(name, Byte.valueOf(o.toString()));
        }
        if (type == Boolean.class) {
            contentValues.put(name, Boolean.valueOf(o.toString()));
        }
        if (type == Float.class) {
            contentValues.put(name, Float.valueOf(o.toString()));
        }
        if (type == Double.class) {
            contentValues.put(name, Double.valueOf(o.toString()));
        }
        if (type == byte[].class) {
            contentValues.put(name, (byte[]) o);
        }

        switch (type.getName()) {
            case "long":
                contentValues.put(name, Long.valueOf(o.toString()));
                break;
            case "int":
                contentValues.put(name, Integer.valueOf(o.toString()));
                break;
            case "boolean":
                contentValues.put(name, Boolean.valueOf(o.toString()));
                break;
            case "float":
                contentValues.put(name, Float.valueOf(o.toString()));
                break;
            case "double":
                contentValues.put(name, Double.valueOf(o.toString()));
                break;
        }
    }

    public static <T extends DataModel> String getTableName(Class<T> clazz) {
        String name = null;

        DbTable annotation = clazz.getAnnotation(DbTable.class);
        if (annotation != null) {
            name = annotation.name();
        }

        return name;
    }

    public static <T extends DataModel> Map.Entry<String, String> getPrimaryKey(T t) {
        Class<? extends DataModel> clazz = t.getClass();
        while (DataModel.class.isAssignableFrom(clazz)) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.getAnnotation(Exclude.class) != null) {
                    continue;
                }
                String name = getColumnName(field);
                boolean isPrimary = (field.getAnnotation(Primary.class) != null);
                if (isPrimary) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    try {
                        Object value = field.get(t);
                        if (field.getType() == byte[].class) {
                            throw new IllegalStateException("Do not use BLOB as primary key");
                        }
                        if (field.getType() == String.class) {
                            value = String.format("'%s'", value);
                        }
                        return new AbstractMap.SimpleEntry<String, String>(name, value.toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            clazz = (Class<T>) clazz.getSuperclass();
        }
        return null;
    }
}
