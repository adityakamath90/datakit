package com.catalyst.datakitlib.core;

import android.content.Context;

/**
 * Created by mukund.d on 16-07-2016.
 */
public class DataKitConfiguration {

    final Context mContext;
    final int mVersion;
    final String mModelPackage;

    private DataKitConfiguration(Context context, int version, String modelPackage) {
        this.mContext = context.getApplicationContext();
        this.mVersion = version;
        this.mModelPackage = modelPackage;
    }

    public static Builder builder(Context context) {
        Builder builder = new Builder(context);
        return builder;
    }

    public void init() {
        DataKitManager.getInstance().init(this, mVersion);
    }

    public static class Builder {

        private final Context context;
        private int version;
        private String modelPackage;

        public Builder(Context context) {
            this.context = context;
        }

        public DataKitConfiguration build() {
           return new DataKitConfiguration(context, version, modelPackage);
        }

        public Builder with(int version) {
            this.version = version;
            return this;
        }

        public Builder with(String modelPackage) {
            this.modelPackage = modelPackage;
            return this;
        }
    }

}
