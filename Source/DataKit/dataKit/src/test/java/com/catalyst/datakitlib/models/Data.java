package com.catalyst.datakitlib.models;

import com.catalyst.datakitlib.DataModel;
import com.catalyst.datakitlib.annotation.DbTable;
import com.catalyst.datakitlib.annotation.Field;

/**
 * Created by mukund.d on 16-07-2016.
 */
@DbTable(name = "data")
public class Data extends DataModel {

    @Field(isNull = false)
    private String text;

    private boolean sectionBreak;

    @Field(isNull = false)
    private int sequence;

    private String link;

    private String path;

    // private SubMenu[] subMenu;

    private String type;

    private String moduleAssoc;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getSectionBreak() {
        return sectionBreak;
    }

    public void setSectionBreak(boolean sectionBreak) {
        this.sectionBreak = sectionBreak;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /*public SubMenu[] getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(SubMenu[] subMenu) {
        this.subMenu = subMenu;
    }*/

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModuleAssoc() {
        return moduleAssoc;
    }

    public void setModuleAssoc(String moduleAssoc) {
        this.moduleAssoc = moduleAssoc;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", sectionBreak = " + sectionBreak + ", sequence = " + sequence + ", link = " + link + ", path = " + path + /*", subMenu = " + subMenu +*/ ", type = " + type + ", moduleAssoc = " + moduleAssoc + "]";
    }

}
