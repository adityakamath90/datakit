package com.catalyst.datakitlib;

import com.catalyst.datakitlib.core.TableMapper;
import com.catalyst.datakitlib.core.data.ColumnProp;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    @Test
    public void queryTableMapper() {
        HashMap<String, List<ColumnProp>> tablesMap = TableMapper.getAllTables("com.catalyst.datakitlib.models", null);
        assertTrue(tablesMap != null && tablesMap.size() > 0);
    }
}